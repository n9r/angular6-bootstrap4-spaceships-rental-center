import { Component, OnInit } from '@angular/core';
import { UserDataService } from '../user-data.service';

import { Spaceship } from '../spaceship';
import { ADDITIONALPACKAGES } from '../mock-data';

@Component({
  selector: 'app-spaceship-details',
  templateUrl: './spaceship-details.component.html',
  styleUrls: ['./spaceship-details.component.css']
})
export class SpaceshipDetailsComponent implements OnInit {

  additionalPackages = ADDITIONALPACKAGES;
  selectedSpaceship: Spaceship;
  selectedData: Object;

  showModal = false;

  constructor(private userDataService: UserDataService) {
    this.selectedSpaceship = this.userDataService.getSpaceship();
    this.selectedData = userDataService.getData();
  }

  ngOnInit() { }

  showSummary(): void {
    this.showModal = true;
  }

  onboardServices(additionalPackages): string {
    let phrase = additionalPackages.beverages === true ? 'beverages, ' : '';
    phrase += additionalPackages.supper === true ? 'supper, ' : '';
    phrase += additionalPackages.hibernation === true ? 'hibernation, ' : '';
    phrase += additionalPackages.astralProjection === true ? 'astral projection, ' : '';

    return phrase.slice(0, -2) + '.';
  }

}
