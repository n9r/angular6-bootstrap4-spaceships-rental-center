export class Spaceship {
    'id': number;
    'name': string;
    'description': string;
    'color': string;
    'size': string;
    'shape': string;
    'speed': string;
    'availableFrom': string;
    'availableTo': string;
}
