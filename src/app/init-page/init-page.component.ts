import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserDataService } from '../user-data.service';
import { Spaceship } from '../spaceship';
import { MOCKSPACESHIPS, PICKUPDROPOFFPLACES } from '../mock-data';

@Component({
  selector: 'app-init-page',
  templateUrl: './init-page.component.html',
  styleUrls: ['./init-page.component.css']
})
export class InitPageComponent implements OnInit {

  spaceships: Array<Spaceship> = MOCKSPACESHIPS;

  pickUpDropOff = PICKUPDROPOFFPLACES;

  spaceshipShape: Array<string>;
  selectedShape = '';
  selectedShapes: Array<string> = [];

  spaceshipSpeed: Array<string>;
  selectedSpeed = '';
  selectedSpeeds: Array<string> = [];

  spaceshipColor: Array<string>;
  selectedColor = '';
  selectedColors: Array<string> = [];

  spaceshipSize: Array<string>;
  selectedSize = '';
  selectedSizes: Array<string> = [];

  inputDateFrom_Parsed = 0;
  inputDateTo_Parsed: number = Number.MAX_SAFE_INTEGER;

  submitIndicator: boolean;

  constructor(
    public userDataService: UserDataService,
    private router: Router
  ) {
    // creating arrays of unique values occuring in given property amongst objects
    this.spaceshipShape = this.returnUniqueValues(this.spaceships, 'shape');
    this.spaceshipSpeed = this.returnUniqueValues(this.spaceships, 'speed');
    this.spaceshipColor = this.returnUniqueValues(this.spaceships, 'color');
    this.spaceshipSize = this.returnUniqueValues(this.spaceships, 'size');


    // displays everything at init
    this.selectedShapes = this.spaceshipShape.map((e) => e);
    this.selectedSpeeds = this.spaceshipSpeed.map((e) => e);
    this.selectedColors = this.spaceshipColor.map((e) => e);
    this.selectedSizes = this.spaceshipSize.map((e) => e);
  }

  ngOnInit() { }

  submitData(dateFrom: string, dateTo: string, pickUp: string, dropOff: string): void {
    const objectToStoreUserData = {
      inputDateFrom: dateFrom,
      inputDateTo: dateTo,
      pickUp: pickUp,
      dropOff: dropOff,
      selectedShapes: this.spaceshipShape,
      selectedSpeeds: this.spaceshipSpeed,
      selectedColors: this.spaceshipColor,
      selectedSizes: this.spaceshipSize
    };

    this.userDataService.storeData(objectToStoreUserData);
    this.submitIndicator = true;

    this.router.navigate(['/SpaceshipSelect']);
  }

  returnUniqueValues(array: Spaceship[], key: string): Array<string> {
    return array.map((e) => e[key]).filter((v, i, a) => a.indexOf(v) === i);
  }

  switchValueInArray(array: Array<string>, index, property): void {
    !array[index] || array[index] === '' ? array[index] = property : array[index] = '';
  }

  setAndParseInputDateFrom(date: string): void {
    this.inputDateFrom_Parsed = Date.parse(date);
  }
  setAndParseInputDateTo(date: string): void {
    this.inputDateTo_Parsed = Date.parse(date);
  }

  parseDate(date: string): number {
    return Date.parse(date);
  }

}
