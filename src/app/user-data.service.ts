import { Injectable } from '@angular/core';
import { Spaceship } from './spaceship';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  userData: Object;
  selectedSpaceship: Spaceship;

  constructor() { }

  storeData(dataObj: Object): void {
    this.userData = [];
    this.userData = dataObj;
  }

  getData(): Object {
    return this.userData;
  }

  setSpaceship(spaceship: Spaceship): void {
    this.selectedSpaceship = spaceship;
  }

  getSpaceship(): Spaceship {
    return this.selectedSpaceship;
  }

}
