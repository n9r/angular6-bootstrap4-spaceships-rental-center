import { Spaceship } from './spaceship';

export const MOCKSPACESHIPS: Spaceship[] = [
    {
        'id': 1,
        'name': 'Cruiser One',
        // tslint:disable-next-line:max-line-length
        'description': 'The Space Shuttle was a partially reusable low Earth orbital spacecraft system operated by the U.S. National Aeronautics and Space Administration (NASA). Its official program name was Space Transportation System (STS), taken from a 1969 plan for a system of reusable spacecraft of which it was the only item funded for development.',
        'color': 'blue',
        'size': 'big',
        'shape': 'cube',
        'speed': 'crawling',
        'availableFrom': '2018-09-25',
        'availableTo': '2019-04-20'
    }, {
        'id': 2,
        'name': 'Snail Two',
        // tslint:disable-next-line:max-line-length
        'description': 'Operational missions launched numerous satellites, conducted science experiments in orbit, and participated in construction and servicing of the International Space Station (ISS). The first of four orbital test flights occurred in 1981, leading to operational flights beginning in 1982.',
        'color': 'blue',
        'size': 'small',
        'shape': 'torus',
        'speed': 'walking',
        'availableFrom': '2018-04-15',
        'availableTo': '2018-09-20'
    }, {
        'id': 3,
        'name': 'Blitz Three',
        // tslint:disable-next-line:max-line-length
        'description': 'From 1981 to 2011 a total of 135 missions were flown, launched from Kennedy Space Center (KSC) in Florida. During that time period the fleet totaled 1,322 days, 19 hours, 21 minutes and 23 seconds of flight time.[2] The longest orbital flight of the shuttle was STS-80 at 17 days 15 hours, while the shortest flight was STS-51-L at one minute 13 seconds when the Space Shuttle Challenger broke apart during launch. ',
        'color': 'green',
        'size': 'little',
        'shape': 'torus',
        'speed': 'light speed',
        'availableFrom': '2018-09-10',
        'availableTo': '2019-09-20'
    }, {
        'id': 4,
        'name': 'Immediator Four',
        // tslint:disable-next-line:max-line-length
        'description': 'The shuttles docked with Russian space station Mir nine times and visited the ISS thirty-seven times. The highest altitude (apogee) achieved by the shuttle was 350 miles when servicing the Hubble Space Telescope.[3] The program flew a total of 355 people representing 16 countries.[4] The Kennedy Space Center served as the landing site for 78 missions, while 54 missions landed at Edwards Air Force Base in California and 1 mission landed at White Sands, New Mexico.',
        'color': 'green',
        'size': 'medium',
        'shape': 'cube',
        'speed': 'light speed',
        'availableFrom': '2019-01-15',
        'availableTo': '2020-05-20'
    }, {
        'id': 5,
        'name': 'FTL Five',
        // tslint:disable-next-line:max-line-length
        'description': 'The first orbiter, Enterprise, was built solely for atmospheric flight tests and had no orbital capability. Four fully operational orbiters were initially built: Columbia, Challenger, Discovery, and Atlantis. Challenger and Columbia were destroyed in mission accidents in 1986 and 2003 respectively, killing a total of fourteen astronauts. A fifth operational orbiter, Endeavour, was built in 1991 to replace Challenger. The Space Shuttle was retired from service upon the conclusion of STS-135 by Atlantis on 21 July 2011.',
        'color': 'green',
        'size': 'big',
        'shape': 'torus',
        'speed': 'warp speed',
        'availableFrom': '2018-02-15',
        'availableTo': '2025-12-20'
    }, {
        'id': 6,
        'name': 'NSFL Six',
        // tslint:disable-next-line:max-line-length
        'description': 'The U.S. Space Shuttle program was officially referred to as the Space Transportation System (STS). Specific shuttle missions were therefore designated with the prefix STS.[2] Initially, the launches were given sequential numbers indicating order of launch, such as STS-7. Subsequent to the Apollo 13 mishap, due to NASA Administrator James M. ',
        'color': 'green',
        'size': 'medium',
        'shape': 'cone',
        'speed': 'warp speed',
        'availableFrom': '2018-12-05',
        'availableTo': '2025-11-20'
    }, {
        'id': 7,
        'name': 'Crazy Seven',
        // tslint:disable-next-line:max-line-length
        'description': 'After the Challenger disaster, NASA returned to using a sequential numbering system, with the number counting from the beginning of the STS program. Unlike the initial system, however, the numbers were assigned based on the initial mission schedule, and did not always reflect actual launch order. This numbering scheme started at 26, with the first flight as STS-26R—the R suffix stood for reflight to disambiguate from prior missions.',
        'color': 'blue',
        'size': 'medium',
        'shape': 'cube',
        'speed': 'ridiculous',
        'availableFrom': '2018-11-01',
        'availableTo': '2021-10-20'
    }, {
        'id': 8,
        'name': 'Don\'t Ask Eight',
        // tslint:disable-next-line:max-line-length
        'description': 'As a result of the changes in systems, flights under different numbering systems could have the same number with one having a letter appended, e.g. flight STS-51 (a mission carried out by Discovery in 1993) was many years after STS-51-A (Discovery\'s second flight in 1984).',
        'color': 'green',
        'size': 'small',
        'shape': 'cylinder',
        'speed': 'ludicrous',
        'availableFrom': '2018-08-16',
        'availableTo': '2018-10-21'
    }, {
        'id': 9,
        'name': 'No Nine',
        // tslint:disable-next-line:max-line-length
        'description': 'All spacecraft except single-stage-to-orbit vehicles cannot get into space on their own, and require a launch vehicle.',
        'color': 'green',
        'size': 'small',
        'shape': 'cone',
        'speed': 'ludicrous',
        'availableFrom': '2018-09-07',
        'availableTo': '2022-09-24'
    }, {
        'id': 10,
        'name': 'To Ten',
        // tslint:disable-next-line:max-line-length
        'description': 'All spacecraft except single-stage-to-orbit vehicles cannot get into space on their own, and require a launch vehicle.',
        'color': 'green',
        'size': 'small',
        'shape': 'cone',
        'speed': 'ludicrous',
        'availableFrom': '2019-01-07',
        'availableTo': '2020-11-04'
    }
];



export const ADDITIONALPACKAGES = {
    'beverages': false,
    'supper': false,
    'hibernation': false,
    'astralProjection': false
};

export const PICKUPDROPOFFPLACES = [
    'Mercury',
    'Venus',
    'Earth',
    'Mars',
    'Jupiter',
    'Saturn',
    'Uranus',
    'Neptune'
];

