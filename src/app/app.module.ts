import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { InitPageComponent } from './init-page/init-page.component';
import { SpaceshipSelectComponent } from './spaceship-select/spaceship-select.component';
import { SpaceshipDetailsComponent } from './spaceship-details/spaceship-details.component';

import { TrimStringPipe } from './trim-string.pipe';


const appRoutes: Routes = [
  {
    path: '',
    component: InitPageComponent
  }, {
    path: 'SpaceshipSelect',
    component: SpaceshipSelectComponent
  }, {
    path: 'SpaceshipDetails',
    component: SpaceshipDetailsComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    InitPageComponent,
    SpaceshipSelectComponent,
    SpaceshipDetailsComponent,
    TrimStringPipe
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
