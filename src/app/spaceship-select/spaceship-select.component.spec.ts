import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpaceshipSelectComponent } from './spaceship-select.component';

describe('SpaceshipSelectComponent', () => {
  let component: SpaceshipSelectComponent;
  let fixture: ComponentFixture<SpaceshipSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpaceshipSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaceshipSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
