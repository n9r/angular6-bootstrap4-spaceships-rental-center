import { Component, OnInit } from '@angular/core';
import { UserDataService } from '../user-data.service';
import { Router } from '@angular/router';

import { Spaceship } from '../spaceship';
import { MOCKSPACESHIPS } from '../mock-data';

@Component({
  selector: 'app-spaceship-select',
  templateUrl: './spaceship-select.component.html',
  styleUrls: ['./spaceship-select.component.css']
})
export class SpaceshipSelectComponent implements OnInit {

  selectedData: Object;
  spaceships: Array<Spaceship> = MOCKSPACESHIPS;

  constructor(private userDataService: UserDataService,
    private router: Router) {
    if (userDataService.getData()) { this.selectedData = userDataService.getData(); }
  }

  ngOnInit() { }

  selectSpaceship(spaceship: Spaceship): void {
    this.userDataService.setSpaceship(spaceship);
    this.router.navigate(['/SpaceshipDetails']);
  }

  parseDate(date: string): number {
    return Date.parse(date);
  }
}
